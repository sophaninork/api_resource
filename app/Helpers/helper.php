<?php

if (!function_exists('name')) {
    function name()
    {
        //
    }
}

if (!function_exists('getConstant')) {
    function getConstant($value = null)
    {
        return config('constants.' . $value);
    }
}