<?php

namespace App\Http\Controllers\Backend;

use Auth;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\AuthRequest;
use App\Repositories\Interfaces\AuthRepositoryInterface;

class AuthController extends Controller
{
    private $authRepository;

    public function __construct(AuthRepositoryInterface $authRepository)
    {
        $this->authRepository = $authRepository;
    }

    /**
     * Persist login
     *
     * @param AuthRequest $request
     *
     * @return mixed
     */

    public function login(AuthRequest $request)
    {
        $response = $this->authRepository->login($request->only(['email','password']));

        if ($response['success']) {
            return $this->successResponse($response);
        }

        return $this->errorResponse($response['message'], [], Response::HTTP_UNAUTHORIZED);
    }

    public function logout()
    {
        return Auth::logout();
    }
}
