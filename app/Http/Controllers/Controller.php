<?php

namespace App\Http\Controllers;

use Illuminate\Http\Response;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    const SUCCESS_STATUS = true;

    public function successResponse(array $data, $status = Response::HTTP_OK)
    {
        return response()->json($data, $status);
    }

    public function successStatusResponse()
    {
        return response()->json(['status' => self::SUCCESS_STATUS], Response::HTTP_OK);
    }
    
    /**
     * @param $message
     * @param $code
     * @return JsonResponse
     */
    public function errorResponse($message, $errorResult = [], $code = Response::HTTP_NOT_FOUND)
    {
        $error = [
            'code' => $code,
            'message' => $message,
        ];

        if (!empty($errorResult)) {
            $error['detail'] = $errorResult;
        }

        $response['error'] = $error;

        return response()->json($response, $code);
    }
}
