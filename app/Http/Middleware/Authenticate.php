<?php

namespace App\Http\Middleware;

use App\Models\User;
use Closure;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Auth\Middleware\Authenticate as Middleware;
use Illuminate\Support\Facades\Auth;

// class Authenticate extends Middleware
// {
//     /**
//      * Get the path the user should be redirected to when they are not authenticated.
//      *
//      * @param  \Illuminate\Http\Request  $request
//      * @return string|null
//      */
//     protected function redirectTo($request)
//     {
//         if (! $request->expectsJson()) {
//             return route('login');
//         }
//     }
// }

class Authenticate
{
    /**
     * Handle an incomming request
     * 
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */

     public function handle($request, Closure $next)
     {
         $token = $request->header('X-Access-Token');
         if (empty($token)) {
             throw new AuthenticationException("Unauthenticated.");
         }

         $user = User::where('access_token', $token)->first();
         if (is_null($user)) {
             throw new AuthenticationException("Unauthenticated.");
         } else {
             Auth::login($user);
             return $next($request);
         }
     }
}