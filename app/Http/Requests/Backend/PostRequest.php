<?php

namespace App\Http\Requests\Backend;

use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Http\FormRequest;

class PostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'user_id' => [
                'required',
                'exists:users,id'
            ],
            'department_id' => [
                'required',
                'exists:departments,id'
            ],
            'title' => [
                'required',
                'min:2',
                'max:256'
            ],
            'body' => [
                'required',
                'min:50',
                'max:8000'
            ]
        ];

        if (Route::current()->getName() == 'api.auth.post.update') {
            $rules += [
                'id' => [
                    'required',
                    'exists:posts, id'
                ],
                'photo' => [
                    'image',
                    'mimes:jpg,png,jpeg'
                ]
            ];
        } else {
            $rules += [
                'photo' => [
                    'required',
                    'image',
                    'mimes:jpg,png,jpeg'
                ]
            ];
        }

        return $rules;
    }
}
