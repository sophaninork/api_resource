<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PostDetailResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => (string) $this->id,
            'user' => $this->user_id,
            'department' => $this->department_id,
            'title' => $this->title,
            'image_url' => $this->image_url,
            'body' => $this->body,
            'status' => $this->status
        ];
    }
}
