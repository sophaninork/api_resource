<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    protected $fillable = [
        'school_id',
        'name',
        'image',
        'tel',
        'email',
        'status'
    ];
}
