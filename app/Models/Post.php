<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Storage;

class Post extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'user_id',
        'department_id',
        'title',
        'image',
        'body',
        'status'
    ];

    protected $appends = [
        'image_url',
    ];

    const STATUS_PUBLISH = 'publish';
    const STATUS_UNPUBLISH = 'unpublish';

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    // scope
    public function scopePublish($query)
    {
        return $query->where('status', self::STATUS_PUBLISH);
    }

    // get attribute
    public function getImageUrlAttribute()
    {
        return ($this->image) ? Storage::url(getConstant('images.post') . $this->image) : '';
    }
}
