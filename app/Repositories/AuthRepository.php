<?php

namespace App\Repositories;

use App\Models\User;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use App\Repositories\Interfaces\AuthRepositoryInterface;

class AuthRepository implements AuthRepositoryInterface
{
    protected $user;

    public function __construct(User $user)
    {
           $this->user = $user;
    }

    /**
     * Persist login
     *
     * @param $attributes
     *
     * @return object
     */
    public function login($attributes)
    {
        $email = $attributes['email'];
        $password = $attributes['password'];

        $user = $this->user->where([
            'email' => $email,
            'status' => getConstant('users.status.active')
        ])->first();

        if ($user && Hash::check($password, $user->password)) {
            $user->access_token = Str::random(80);
            $user->save();

            return [
                'success' => true,
                'token' => $user->access_token,
            ];
        }

        return $this->loginFailed();
    }

    /**
     * Will return case unauthorized
     *
     * @return object
     */
    private function loginFailed()
    {
        return [
            'success' => false,
            'message' => __('auth.failed')
        ];
    }
}