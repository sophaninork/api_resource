<?php

namespace App\Repositories\Interfaces;

interface PostRepositoryInterface
{
    /**
     * Get all
     * 
     * @param int $limit
     * 
     * @return mixed
     */
     public function getList(int $limit);

     /**
      * Persist new data into storage 
      *
      * @param object $attributes
      * 
      * @return boolean
      */
      public function create($attributes);

      /**
       * Get detail
       * 
       * @param int $id
       * 
       * @return array
       */
      public function show($id);

      /**
       * Update data
       * 
       * @param int $id
       * @param object $attributes
       * 
       * @return boolean
       */
      public function update($id, $attributes);

      /**
       * Delete
       * 
       * @param int $id
       * 
       * @return boolean
       */
      public function destroy($id);
}