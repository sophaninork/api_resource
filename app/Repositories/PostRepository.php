<?php

namespace App\Repositories;

use App\Models\Post;
use App\Models\Department;
use App\Models\User;
use App\Traits\FileUploadTrait;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Repositories\Interfaces\PostRepositoryInterface;

class PostRepository implements PostRepositoryInterface
{
    use FileUploadTrait;

    protected $path;
    protected $post;
    protected $department;

    public function __construct(
        Post $post,
        User $user,
        Department $department
    )
    {
        $this->post = $post;
        $this->path = getConstant('images.path');
    }

    public function getList($limit)
    {
        $userID = request('user_id', null);
        $departmentID = request('department_id', null);
        $searchText = request('searchText', null);

        return $this->post->when($searchText, function ($q, $text) {
            return $q->orWhere('title', 'LIKE', '%' . $text . '%');
        })
        ->when($departmentID, function ($query, $departmentID) {
            return $query->orWhere('department_id', $departmentID);
        })
        ->when($userID, function ($q, $userID) {
            return $q->orWhere('user_id', $userID);
        })
        ->latest()
        ->paginate($limit);
    }

    public function create($request)
    {
        $this->isImageExist($request);
        return $this->post->create($request->except('photo'));
    }

    public function show($id)
    {
        return $this->post->findOrFail($id);
    }

    public function update($id, $attributes)
    {
        $this->isImageExist($attributes);
        return tap($this->post->findOrFail($id))->update($attributes->except('photo'));
    }

    public function destroy($id)
    {
        $post = $this->post->find($id);
        if (!is_null($post)) {
            if (!empty($post->image)) {
                $this->deleteFile($this->path, $post->image);
            }    
            $post->delete();

            return false;
        }

        throw new ModelNotFoundException('Post Not Found!');
    }

    public function isImageExist($request)
    {
        $oldImg = $this->post->whereId($request['id'])->pluck('image');

        if ($request->hasFile('photo')) {
            if (!empty($oldImg)) {
                $this->deleteFile($this->path, $oldImg);
            }

            $img = $this->uploadFile($this->path, $request['photo']);

            return $request['image'] = $img;
        }
    }
}