<?php

namespace App\Traits;

use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Storage;

trait FileUploadTrait
{
    /**
     * Upload file to storage
     * 
     * @param string path
     * @param object filename
     * 
     * @return string
     */

     public function uploadFile($path = null, $filename = null)
     {
         $image = Image::make($filename);
         $extension = $filename->extension();
         $file = Str::random(20) . time() . '.' . $extension;
         Storage::put($path . $file, $image->orientate()->encode($extension), 'public');

         return $file;
     }

     /**
      * Delete file from storage
      *
      * @param string $path
      * @param mixed $filename
      *
      *@return boolean
      */
     public function deleteFile($path = null, $filename = null)
     {
         $filePath = collect($filename)->map(function ($item) use ($path) {
             return $path . $item;
         })->toArray();

         Storage::delete($filePath);
     }
}