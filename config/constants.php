<?php

return [
    'users' => [
        'status' => [
            'active' => 'active',
            'inactive' => 'inactive'
        ]
    ],
    'images' => [
        'path' => 'public/'
    ],
    'departments' => [
        'status' => [
            'publish' => 'publish',
            'unpublish' => 'unpublish'
        ]
    ],
    'limit_per_page' => 10,
];