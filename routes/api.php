<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('v1/auth/login', 'Backend\AuthController@login')->name('auth.login');

Route::namespace('Backend')
       ->prefix('v1/auth')
       ->middleware('auth:api')
       ->group(function () {
           Route::get('/logout', 'AuthController@logout')->name('auth.logout');
           Route::resource('post', 'PostController')
           ->except([
               'update',
               'edit',
           ]);
           Route::post('/post/update/', 'PostController@update')->name('post.update');
       });